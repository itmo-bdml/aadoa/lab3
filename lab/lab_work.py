import random

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy.optimize import minimize, curve_fit
matplotlib.use('TkAgg')

random.seed(42)
np.random.seed(42)
EPSILON = 0.001


def linear(x, a: float, b: float):
    return a * x + b


def rational(x, a: float, b: float):
    return a / (1 + b * x)


def mse_loss(f: callable, a: float, b: float, x, predictions) -> float:
    l: float = 0
    for x_val, prediction in zip(x, predictions):
        l += (f(x_val, a, b) - prediction)**2
    return l


def mse_loss_linear(smth):
    a, b = smth
    return mse_loss(linear, a, b, x, y)


def mse_loss_rational(smth):
    a, b = smth
    return mse_loss(rational, a, b, x, y)


def linear_derivative(coefs):
    a_, b_ = coefs
    return sum(2 * x * (a_ * x + b_ - y)), sum(2 * (a_ * x + b_ - y))


def rational_derivative(coefs):
    a_, b_ = coefs
    return sum(2 * (a_ / (1 + b_ * x) - y) / (1 + b_ * x)), sum(2 * (a_ / (1 + b_ * x) - y) * (- x * a_ / (1 + b_ * x) ** 2))


def gradient_descent(fun: callable, initial=[0, 0], lr=0.08, epsilon=0.001, show_res=False):
    res = {}
    a_, b_ = initial
    loss_array = []
    params = []
    for i in range(10000):
        loss = np.sum((a_ * x + b_ - y) ** 2)
        der = fun((a_, b_))
        a_ = a_ - lr * der[0] / 101
        b_ = b_ - lr * der[1] / 101
        loss_array.append(loss)
        params.append((a_, b_))

        if i != 0 and abs(loss - loss_array[-2]) < epsilon:
            break

        if not i % 100 and show_res:
            print(f" epoch: {i}, {a_}, {b_}, {loss}")
    res["loss"] = loss_array
    res["x"] = params
    return res


a = np.random.uniform()
b = np.random.uniform()
x = np.arange(0, 101, 1) / 100
y = np.array(a * x + b + np.random.normal(scale=1, size=101))

print(f"a: {a}, b: {b}")
gd_linear = gradient_descent(linear_derivative)
gd_rational = gradient_descent(rational_derivative)

print(f"linear gd: {gd_linear['x'][-1]}, {len(gd_linear['x'])}")
print(f"rational gd: {gd_rational['x'][-1]}, {len(gd_rational['x'])}")


def linear_jacobian(coeffs):
    a, b = coeffs
    return np.array([np.sum(2 * x * (b + a * x - y)),
                     np.sum(2 * (b + a * x - y))])


def rational_jacobian(coeffs):
    a, b = coeffs
    return np.array([np.sum(2/(1+b*x) * (a / (1 + b*x) - y)),
                     np.sum(-2*a*x/(1+b*x)**2 * (a/(1+b*x)-y))]).T


# Conjugate Gradient Descent
cgd_linear = minimize(mse_loss_linear, [0., 0.], method='CG', tol=1e-3, jac=linear_jacobian)
cgd_rational = minimize(mse_loss_rational, [0., 0.], method='CG', tol=1e-3, jac=rational_jacobian)


print("linear Conjugate GD: f(x)=%.2fx + %.2f, loss = %.4f, iterations=%d, f-calculations=%d"
      % (cgd_linear.x[0], cgd_linear.x[1], cgd_linear.fun, cgd_linear.nit, cgd_linear.nfev))
print("rational Conjugate GD: f(x) = %.2fx / (1 + %.2fx), loss = %.4f, iterations=%d, f-calculations=%d"
      % (cgd_rational.x[0], cgd_rational.x[1], cgd_rational.fun, cgd_rational.nit, cgd_rational.nfev))


def linear_loss_all(coeffs):
    a, b = coeffs
    return np.sum((a * x + b - y)**2)


def linear_hessian(coeffs):
    return np.array([[np.sum(2*x*x),
                      np.sum(2*x)],
                     [np.sum(2*x),
                      2]])


def rational_loss_all(coeffs):
    a, b = coeffs
    return np.sum((a/(1+b*x) - y)**2)


def rational_hessian(coeffs):
    a, b = coeffs
    return np.array([[np.sum(2 / (1+b*x)** 2),
                      np.sum(-4*a*x / (1+b*x)**3 + 2*y*x / (1+b*x)**2)],
                     [np.sum(-4*a*x / (1+b*x)**3 + 2*y*x / (1+b*x)**2),
                      np.sum(2*a*x / (1+b*x)**3 * (3*a/(1+b*x) - 2/y))]])


newton_linear = minimize(linear_loss_all, [0.0, 0.0], method='Newton-CG', jac=linear_jacobian, hess=linear_hessian, tol=1e-3)
newton_rational = minimize(rational_loss_all, [0.0, 0.0], method='Newton-CG', jac=rational_jacobian, hess=rational_hessian, tol=1e-3)

print("linear Newton's method: f(x)=%.2fx + %.2f, loss = %.4f, iterations=%d, f-calculations=%d"
      % (newton_linear.x[0], newton_linear.x[1], newton_linear.fun, newton_linear.nit, newton_linear.nfev))
print("rational Newton's method: f(x) = %.2fx / (1 + %.2fx), loss = %.4f, iterations=%d, f-calculations=%d"
      % (newton_rational.x[0], newton_rational.x[1], newton_rational.fun, newton_rational.nit, newton_rational.nfev))


# Levenberg-Marquardt algorithm
lma_linear = curve_fit(linear, x, y, [0, 0], full_output=True, method='lm')
lma_rational = curve_fit(rational, x, y, [0., 0.0], full_output=True, method='lm')

# print(lma_linear)
print("linear LMA: f(x)=%.2fx + %.2f, f-calculations=%d"
      % (lma_linear[0][0], lma_linear[0][1], lma_linear[2]['nfev']))
print("rational LMA: f(x) = %.2fx / (1 + %.2fx), f-calculations=%d"
      % (lma_rational[0][0], lma_rational[0][1], lma_rational[2]['nfev']))


plt.figure(figsize=(12, 6))
plt.subplot(121)
plt.title("Linear")
plt.scatter(x, y)
plt.plot(x, linear(x, a, b), label="expected linear")
plt.plot(x, gd_linear['x'][-1][0] * x + gd_linear['x'][-1][1], label="predicted GD")
plt.plot(x, cgd_linear.x[0] * x + cgd_linear.x[1], label="predicted CGD")
plt.plot(x, newton_linear.x[0] * x + newton_linear.x[1], label="predicted Newton")
plt.plot(x, lma_linear[0][0] * x + lma_linear[0][1], label="predicted lma")
plt.legend(loc="best")

plt.subplot(122)
plt.scatter(x, y)
plt.title("Rational")
plt.plot(x, linear(x, a, b), label="expected linear")
plt.plot(x, gd_rational['x'][-1][0] / (1 + gd_rational['x'][-1][1] * x), label="predicted GD")
plt.plot(x, cgd_rational.x[0] / (1 + cgd_rational.x[1] * x), label="predicted CGD")
plt.plot(x, newton_rational.x[0] / (1 + newton_rational.x[1] * x), label="predicted Newton")
plt.plot(x, lma_rational[0][0] / (1 + lma_rational[0][1] * x), label="predicted LMA")
plt.legend(loc="best")
plt.savefig("./plots/plot.png")

plt.show()
