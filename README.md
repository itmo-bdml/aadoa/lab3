## Lab 3

### Algorithms for unconstrained nonlinear optimization. First- and second- order methods

---
[Report (google doc)](https://docs.google.com/document/d/1sl8sGDiU-GvvUjsY82Hk4gtXHec_q0uLpf7WbAdkJeI)

Performed by:
* Elya Avdeeva
* Gleb Mikloshevich

**plot**

<img src="./lab/plots/plot.png" height="250">
